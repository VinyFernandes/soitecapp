package com.soirtec.soirtecapp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinicius on 25/07/18.
 */
public class MoviesResponse implements Serializable {

    private final static long serialVersionUID = 9081163961107076989L;

    @SerializedName("Search")
    @Expose
    private List<Movie> movies = null;
    @SerializedName("totalResults")
    @Expose
    private String totalResults;
    @SerializedName("Response")
    @Expose
    private String response;

    public MoviesResponse() {
    }

    public MoviesResponse(List<Movie> movies, String totalResults, String response) {
        this.movies = movies;
        this.totalResults = totalResults;
        this.response = response;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "MoviesResponse{" +
                "movies=" + movies +
                ", totalResults='" + totalResults + '\'' +
                ", response='" + response + '\'' +
                '}';
    }

}
