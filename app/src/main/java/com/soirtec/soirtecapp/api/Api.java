package com.soirtec.soirtecapp.api;

/**
 * Created by vinicius on 25/07/18.
 */
public class Api {

    private Api() {}

    public static final String API_KEY = "40f3981";
    public static final String BASE_URL = "http://www.omdbapi.com";

    public static Service getService() {
        return Client.getClient(BASE_URL).create(Service.class);
    }

}
