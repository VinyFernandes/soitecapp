package com.soirtec.soirtecapp.api;

import com.soirtec.soirtecapp.model.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by vinicius on 25/07/18.
 */
public interface Service {

    @GET("/?")
    Call<MoviesResponse> getMovies(@Query("apikey") String apikey, @Query("s") String s, @Query("page") Integer pageIndex);

    @GET("/?")
    Call<MoviesResponse> getDetailMovie(@Query("apikey") String apikey, @Query("i") Integer i);

}
